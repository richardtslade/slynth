#ifndef IMIDIIN_HPP
#define IMIDIIN_HPP

#include <memory>
#include <queue>
#include <array>

namespace midi
{
class IMidiIn
{
public:
	using RawMessage = std::array<uint8_t, 4>;
	using RawMessages =
		std::queue<RawMessage>;

	IMidiIn() = default;
	virtual ~IMidiIn() = default;

	virtual bool HasMessage() = 0;
	virtual RawMessage GetNextMessage() = 0;
	virtual bool Start() = 0;

protected:
	virtual void PollForMessage() = 0;

	RawMessages m_rawMessages;
};

using IMidiInUP =
	std::unique_ptr<IMidiIn>;
} // namespace midi

#endif // IMIDIIN_HPP
