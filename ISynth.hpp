#ifndef ISYNTH_HPP
#define ISYNTH_HPP

#include <memory>

#include "MidiMessage.hpp"

namespace synth
{
class ISynth
{
public:
	ISynth() = default;
	virtual ~ISynth() = default;

	virtual void PushMidiMessage(midi::MidiMessage&& msg) = 0;

protected:
	midi::MidiMessages m_messages;
};

using ISynthSP =
	std::shared_ptr<ISynth>;

} // namespace synth

#endif // ISYNTH_HPP
