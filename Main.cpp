#include <iostream>

#include "Tests.hpp"
#include "MessageTranslator.hpp"
#include "MidiIn.hpp"
#include "Synth.hpp"

int main()
{
	if(tests::RunTests())
	{
		std::cout << "Tests passed!" << std::endl;

		auto mockMidiIn =
		std::unique_ptr<midi::MidiIn>(
			new midi::MidiIn{});

		midi::MessageTranslator translator{std::move(mockMidiIn)};

		auto mockSynth =
			std::shared_ptr<synth::Synth>(
				new synth::Synth{});

		translator.AddSynthesiser(mockSynth);
		translator.Start();
	}

	return 0;
}
