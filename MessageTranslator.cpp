#include <cassert>

#include "MessageTranslator.hpp"

namespace midi
{
MessageTranslator::MessageTranslator(IMidiInUP&& midiIn)
	: m_midiIn(std::move(midiIn))
{}

void MessageTranslator::AddSynthesiser(const synth::ISynthSP& synth)
{
	m_synth = synth;
}

void MessageTranslator::Start()
{
	assert(m_midiIn);

	if(m_midiIn->Start())
	{
		while(m_midiIn->HasMessage())
		{
			const auto nextMessage = m_midiIn->GetNextMessage();
			m_synth->PushMidiMessage(TranslateMessage(nextMessage));
		}
	}
}

/*static*/ uint8_t MessageTranslator::GetTopNibble(const uint8_t byte)
{
	return byte >> 4;
}

/*static*/ uint8_t MessageTranslator::GetBottomNibble(const uint8_t byte)
{
	return byte & 0x0F;
}

/*static*/ bool MessageTranslator::IsStatusByte(const uint8_t msb)
{
	return msb >= static_cast<uint8_t>(MidiMessage::MessageType::NoteOff);
}

/*static*/ std::array<uint8_t, 4u> MessageTranslator::MessageToBytes(const uint32_t& msg)
{
	std::array<uint8_t, 4u> ret;

	static const uint8_t numberBytes = 32u / 8u;
	static const uint8_t numToReduceShift = 8u;

	uint8_t numToShift = 24u;

	for(uint8_t index = 0u; index < numberBytes; ++index)
	{
		const uint8_t byte = static_cast<uint8_t>(msg >> numToShift);
		ret[index] = byte;

		numToShift -= numToReduceShift;
	}

	return ret;
}

MidiMessage MessageTranslator::TranslateMessage(const IMidiIn::RawMessage& rawMsg)
{
	using MessageType = MidiMessage::MessageType;

	assert(rawMsg.size() == 4u);
	const auto mostSignificantByte =
		rawMsg[0];

	const auto msgType =
	[&mostSignificantByte]()
	{
		if(IsStatusByte(mostSignificantByte))
		{
			return static_cast<MessageType>(GetTopNibble(mostSignificantByte));
		}
		else // Just data bytes for previous status, used for compression
		{
			return MessageType::Data;
		}
	}();

	const uint8_t channelNumber =
		[&mostSignificantByte, &msgType]()
	{
		return msgType == MessageType::Data ?
			0u : GetBottomNibble(mostSignificantByte);
	}();

	MidiMessage::MessageData data;

	// Fill data
	switch (msgType)
	{
	case MessageType::Data:
	{
		data.insert(data.begin(), rawMsg.begin(), rawMsg.end());
		break;
	}
	case MessageType::NoteOff:// [[fallthrough]]
	case MessageType::NoteOn:// [[fallthrough]]
	case MessageType::PolymorphicAftertouch:// [[fallthrough]]
	case MessageType::ControlChange:
	case MessageType::PitchBend:
	{
		data.emplace_back(rawMsg[1]);
		data.emplace_back(rawMsg[2]);
		break;
	}
	case MessageType::ProgramChange:// [[fallthrough]]
	case MessageType::AfterTouch:
	{
		data.emplace_back(rawMsg[1]);
		break;
	}
	case MessageType::System:
	{
		// Ignore for now
		break;
	}
	}

	return MidiMessage{msgType, channelNumber, std::move(data)};
}
} // namespace midi
