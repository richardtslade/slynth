#ifndef MESSAGETRANSLATOR_HPP
#define MESSAGETRANSLATOR_HPP

#include <array>

#include "IMidiIn.hpp"
#include "ISynth.hpp"
#include "MidiMessage.hpp"

namespace midi
{
class MessageTranslator
{
public:
	MessageTranslator(IMidiInUP&& midiIn);
	~MessageTranslator() = default;

	void AddSynthesiser(const synth::ISynthSP& synth);
	void Start();

private:
	static uint8_t GetMostSignificantByte(const uint32_t& msg);
	static uint8_t GetTopNibble(const uint8_t byte);
	static uint8_t GetBottomNibble(const uint8_t byte);
	static bool IsStatusByte(const uint8_t msb);
	static MidiMessage::MessageType GetMessageType(const uint8_t statusByte);
	static uint8_t GetByteAtIndex(const uint32_t& msg, const uint8_t byteIndex);

	static std::array<uint8_t, 4u> MessageToBytes(const uint32_t& msg);

	MidiMessage TranslateMessage(const IMidiIn::RawMessage& rawMsg);
	IMidiInUP m_midiIn;
	synth::ISynthSP m_synth;
};
} // namespace midi

#endif // MESSAGETRANSLATOR_HPP
