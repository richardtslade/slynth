#include <iostream>

#include "MidiIn.hpp"

namespace midi
{
MidiIn::~MidiIn()
{
	if(m_alsaMidiIn)
	{
		snd_rawmidi_close(m_alsaMidiIn);
		m_alsaMidiIn = nullptr;
	}
}

bool MidiIn::HasMessage()
{
	return !m_rawMessages.empty();
}

IMidiIn::RawMessage MidiIn::GetNextMessage()
{
	const auto nextMessage = m_rawMessages.front();
	m_rawMessages.pop();
	return nextMessage;
}

bool MidiIn::Start()
{
	long int status;
	int mode = SND_RAWMIDI_SYNC;
	const char* portname = "hw:0,0,0";

	if((status = snd_rawmidi_open(&m_alsaMidiIn, nullptr, portname, mode)) < 0)
	{
		std::cerr << "Status: " << status << " occured when opening midi" << std::endl;
		return false;
	}
	else
	{
		return true;
	}
}

void MidiIn::PollForMessage()
{
	while(true)
	{
		long int status;
		const uint8_t numBytesToRead = 4u;
		uint8_t buffer[numBytesToRead];

		if((status = snd_rawmidi_read(m_alsaMidiIn, buffer, numBytesToRead)) < 0)
		{
			std::cerr << "Status: " << status << " occured when reading midi" << std::endl;
		}
		else
		{
			m_rawMessages.push(
				std::array<uint8_t, numBytesToRead>{
					buffer[0],
					buffer[1],
					buffer[2],buffer[3]});
		}
	}
}
} // namespace midi
