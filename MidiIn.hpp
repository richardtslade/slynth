#ifndef MIDIIN_HPP
#define MIDIIN_HPP

#include <alsa/asoundlib.h>

#include "IMidiIn.hpp"

namespace midi
{
class MidiIn: public IMidiIn
{
public:
	MidiIn() = default;
	~MidiIn() override final;

	bool HasMessage() override final;
	RawMessage GetNextMessage() override final;
	bool Start() override final;

private:
	virtual void PollForMessage();

	snd_rawmidi_t* m_alsaMidiIn = nullptr;
};
} //namespace midi

#endif // MIDIIN_HPP
