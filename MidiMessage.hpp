#ifndef MIDIMESSAGE_HPP
#define MIDIMESSAGE_HPP

#include <vector>
#include <queue>

namespace midi
{
struct MidiMessage
{
public:
	enum class MessageType: uint8_t
	{
		Data = 0x7,
		NoteOff = 0x8,
		NoteOn = 0x9,
		PolymorphicAftertouch = 0xA,
		ControlChange = 0xB,
		ProgramChange = 0xC,
		AfterTouch = 0xD,
		PitchBend = 0xE,
		System = 0xF
	};

	using MessageData =
		std::vector<uint8_t /*Data byte*/>;

	MidiMessage(
		const MessageType type,
		const uint8_t channelNumber,
		MessageData&& data)
		: m_type{type}
		, m_channelNumber{channelNumber}
		, m_data{std::move(data)}
	{}

	MidiMessage() = delete;
	~MidiMessage() = default;

	MessageType m_type;
	uint8_t m_channelNumber;
	MessageData m_data;
};

using MidiMessages =
	std::queue<MidiMessage>;
} //namespace midi

#endif // MIDIMESSAGE_HPP
