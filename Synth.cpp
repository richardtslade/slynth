#include <iostream>
#include <sstream>
#include <cassert>

#include "Synth.hpp"

namespace
{
std::string MessageTypeToString(midi::MidiMessage::MessageType type)
{
	using MessageType =
		midi::MidiMessage::MessageType;

	switch(type)
	{
	case MessageType::NoteOff: return std::string{"NoteOff"};
	case MessageType::NoteOn: return std::string{"NoteOn"};
	case MessageType::PolymorphicAftertouch: return std::string{"PolymorphicAftertouch"};
	case MessageType::ControlChange: return std::string{"ControlChange"};
	case MessageType::ProgramChange: return std::string{"ProgramChange"};
	case MessageType::AfterTouch: return std::string{"AfterTouch"};
	case MessageType::PitchBend: return std::string{"PitchBend"};
	case MessageType::System: return std::string{"System"};
	case MessageType::Data: return std::string{"Data"};
	}

	return std::string{};
}

std::string MessageDataToString(
	const midi::MidiMessage::MessageType messageType,
	const midi::MidiMessage::MessageData& data)
{
	using MessageType =
		midi::MidiMessage::MessageType;

	std::stringstream ss;

	switch (messageType)
	{
	case MessageType::Data:
	{
		assert(data.size() == 4u);
		ss
		<< "Byte 1: " << std::to_string(data[0])
		<< ", Bye2: " << std::to_string(data[1])
		<< ", Bye3: " << std::to_string(data[2])
		<< ", Bye4: " << std::to_string(data[3]);

		break;
	}
	case MessageType::NoteOff:// [[fallthrough]]
	case MessageType::NoteOn:// [[fallthrough]]
	case MessageType::PolymorphicAftertouch:
	{
		assert(data.size() == 2u);
		ss
		<< "Note: " << std::to_string(data[0])
		<< ", Velocity: " << std::to_string(data[1]);

		break;
	}
	case MessageType::ControlChange:
	{
		assert(data.size() == 2u);
		ss
		<< "Control number: " << std::to_string(data[0])
		<< ", Control Value: " << std::to_string(data[1]);
		break;
	}
	case MessageType::ProgramChange:
	{
		assert(data.size() == 1u);
		ss
		<< "Program number: " << std::to_string(data[0]);
		break;
	}
	case MessageType::AfterTouch:
	{
		assert(data.size() == 1u);
		ss
		<< "Pressure: " << std::to_string(data[0]);
		break;
	}
	case MessageType::PitchBend:
	{
		assert(data.size() == 2u);

		ss
		<< "Bend: " << std::to_string((data[1] << 8) + data[0]);
		break;
	}
	case MessageType::System:
	{
		// Ignore for now
		break;
	}
	}

	return ss.str();
}

void PrintMessage(const midi::MidiMessage& msg)
{
	std::cout
	<< "Channel " << std::to_string(msg.m_channelNumber) << ":\t"
	<< MessageTypeToString(msg.m_type)
	<< " " << MessageDataToString(msg.m_type, msg.m_data)
	<< std::endl;
}
} // namespace anon

namespace synth
{
void Synth::PushMidiMessage(midi::MidiMessage&& msg)
{
	PrintMessage(msg);
}
} // namespace synth
