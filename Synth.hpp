#ifndef SYNTH_HPP
#define SYNTH_HPP

#include "ISynth.hpp"

namespace synth
{
class Synth: public synth::ISynth
{
public:
	Synth() = default;
	~Synth() override = default;

	void PushMidiMessage(midi::MidiMessage&& msg) override final;
};
} // namespace synth
#endif // SYNTH_HPP
