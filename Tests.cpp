#include <iostream>
#include <sstream>
#include <cassert>

#include "Tests.hpp"
#include "IMidiIn.hpp"
#include "ISynth.hpp"
#include "MessageTranslator.hpp"

namespace tests
{
using MidiMessage =
		midi::MidiMessage;

class MockMidiIn: public midi::IMidiIn
{
public:
	MockMidiIn(const midi::IMidiIn::RawMessages&& testMessages)
	{
		m_rawMessages = testMessages;
	}

	~MockMidiIn() override final = default;

	bool HasMessage() override final { return !m_rawMessages.empty(); }

	RawMessage GetNextMessage() override
	{
		const auto nextMessage = m_rawMessages.front();
		m_rawMessages.pop();
		return nextMessage;
	}

	bool Start() override final
	{
		return true;
	}

private:
	void PollForMessage() override final {}
};

class MockSynth: public synth::ISynth
{
public:
	MockSynth() = default;
	~MockSynth() override final = default;

	const midi::MidiMessages& GetMessages() const
	{
		return m_messages;
	}

	void PushMidiMessage(MidiMessage&& msg) override final
	{
		m_messages.push(std::move(msg));
	}
};

bool RunMessageTranslatorTests()
{
	const midi::IMidiIn::RawMessages testMessages(
	{
		{0x80, 0x3C, 0x10, 0x00}, // Note off for channel 0, note C3 (middle C) with velocity 16
		{0x91, 0x3E, 0x20, 0x00}, // Note on for channel 1, note D#3 with velocity 32
		{0xA3, 0x3C ,0x11, 0x00}, // PolymorphicAftertouch for channel 4, note C3 with pressure 17
		{0xBF, 0x0A, 0xA0, 0x00}, // Control change for channel 15, controller # 10 with value 160
		{0xCE, 0xA9, 0x00, 0x00}, // ProgramChange for channel 14, program # 169
		{0xD9, 0xB9, 0x10, 0x00}, // Aftertouch for channel 9, program # 186
		{0xE7, 0x40, 0x20, 0x00}, // Pitchbend for channel 7
		{0x7F, 0x40, 0x20, 0x10}  // Pure data message
	});

	auto mockMidiIn =
		std::unique_ptr<MockMidiIn>(
			new MockMidiIn{std::move(testMessages)});

	midi::MessageTranslator translator{std::move(mockMidiIn)};

	auto mockSynth =
		std::shared_ptr<MockSynth>(
			new MockSynth{});

	translator.AddSynthesiser(mockSynth);
	translator.Start();

	auto messages =
		mockSynth->GetMessages();

	assert(messages.size() == testMessages.size());

	auto currentMessage = messages.front();
	messages.pop();

	using MessageType = MidiMessage::MessageType;

	// Test 1
	// 0x80,0x3C,0x10: Note off for channel 0, note C3 (middle C) with velocity 16
	// Status byte
	assert(currentMessage.m_type == MessageType::NoteOff);
	assert(currentMessage.m_channelNumber == 0u);

	auto& data = currentMessage.m_data;
	uint8_t expectedDataSize = 2u;

	assert(data.size() == expectedDataSize);
	assert(data[0] == 0x3C);
	assert(data[1] == 0x10);

	currentMessage = messages.front();
	messages.pop();

	// Test 2
	// 0x91,0x3E,0x20: Note on for channel 1, note D#3 with velocity 32
	// Status byte
	assert(currentMessage.m_type == MessageType::NoteOn);
	assert(currentMessage.m_channelNumber == 1u);

	data = currentMessage.m_data;
	expectedDataSize = 2u;

	assert(data.size() == expectedDataSize);
	assert(data[0] == 0x3E);
	assert(data[1] == 0x20);

	currentMessage = messages.front();
	messages.pop();

	// Test 3
	// 0xA3,0x3C,0x11: PolymorphicAftertouch for channel 3, note C3 with pressure 17
	assert(currentMessage.m_type == MessageType::PolymorphicAftertouch);
	assert(currentMessage.m_channelNumber == 3u);

	data = currentMessage.m_data;
	expectedDataSize = 2u;

	assert(data.size() == expectedDataSize);
	assert(data[0] == 0x3C);
	assert(data[1] == 0x11);

	currentMessage = messages.front();
	messages.pop();

	// Test 4
	// 0xBF,0x0A,0xA0: Control change for channel 15, controller # 10 with value 160
	assert(currentMessage.m_type == MessageType::ControlChange);
	assert(currentMessage.m_channelNumber == 15u);

	data = currentMessage.m_data;
	expectedDataSize = 2u;

	assert(data.size() == expectedDataSize);
	assert(data[0] == 0x0A);
	assert(data[1] == 0xA0);

	currentMessage = messages.front();
	messages.pop();

	// Test 5
	// 0xCE,0xA9: ProgramChange for channel 14, program # 169
	assert(currentMessage.m_type == MessageType::ProgramChange);
	assert(currentMessage.m_channelNumber == 14u);

	data = currentMessage.m_data;
	expectedDataSize = 1u;

	assert(data.size() == expectedDataSize);
	assert(data[0] == 0xA9);

	currentMessage = messages.front();
	messages.pop();

	// Test 6
	// 0xD9,0xB9: Aftertouch for channel 9, program # 186
	assert(currentMessage.m_type == MessageType::AfterTouch);
	assert(currentMessage.m_channelNumber == 9u);

	data = currentMessage.m_data;
	expectedDataSize = 1u;

	assert(data.size() == expectedDataSize);
	assert(data[0] == 0xB9);

	currentMessage = messages.front();
	messages.pop();

	// Test 7
	// 0xE7,0x40,0x20: Pitchbend for channel 7
	assert(currentMessage.m_type == MessageType::PitchBend);
	assert(currentMessage.m_channelNumber == 7u);

	data = currentMessage.m_data;
	expectedDataSize = 2u;

	assert(data.size() == expectedDataSize);
	assert(data[0] == 0x40);
	assert(data[1] == 0x20);

	currentMessage = messages.front();
	messages.pop();

	// Test 7
	// 0xF7,0x40,0x20,0x10 Pure data message
	assert(currentMessage.m_type == MessageType::Data);
	assert(currentMessage.m_channelNumber == 0u);

	data = currentMessage.m_data;
	expectedDataSize = 4u;

	assert(data.size() == expectedDataSize);
	assert(data[0] == 0x7F);
	assert(data[1] == 0x40);
	assert(data[2] == 0x20);
	assert(data[3] == 0x10);

	return true;
}

bool RunTests()
{
	bool result = true;

	result &= RunMessageTranslatorTests();

	return result;
}
} // namespace tests
